Um aplicativo em react que trás as statisticas do covid.

## Installation

- git clone https://github.com/sambreen27/covid19.git
- cd covid19
- npm install
- npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## COVID-19 API - Test

GET https://covid19.mathdro.id/api -> global cases information

GET https://covid19.mathdro.id/api/countries -> countries that are available in the API

GET https://covid19.mathdro.id/api/countries/{countryName} -> get specific country's covid-19 cases data

API provided by: https://github.com/mathdroid/covid-19-api

## Tech Stack

- [React JS](https://github.com/facebook/react)
- [Axios](https://github.com/axios/axios)
- [Charts.js](https://github.com/chartjs/Chart.js)
- [material-ui](https://github.com/mui-org/material-ui)
- [react-particles-js](https://github.com/Wufe/react-particles-js)
- [react-countup](https://github.com/glennreyes/react-countup)

## Preview

- ![Global Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid1.png)
  ![Global Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid2.png)
  ![Specific Country Data](https://github.com/sambreen27/covid19/blob/master/src/images/covid3.png)

### Code

#### Install Jenkins AWS EC2

```sh
#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install epel -y
sudo yum install daemonize -y
sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum upgrade
sudo amazon-linux-extras install java-openjdk11 -y
sudo yum install jenkins -y
sudo systemctl daemon-reload
sudo systemctl start jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

```

Arquivos - Aula 2

```sh
#!/bin/bash
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install docker.io git -y
sudo usermod -aG docker ubuntu
sudo reboot
```

> docker run -itd -p 80:3000 dcw5/app-dcw5

# Rodar na instância Jenkins

sudo yum update -y # Atualiza os softwares disponiveis para o sistema
sudo yum install git -y # Instala o git
sudo amazon-linux-extras install docker -y # Instala o docker
sudo systemctl start docker # Inicia o docker
sudo usermod -aG docker jenkins # Da permissões ao Jenkins para rodar comandos Docker
sudo systemctl restart jenkins # Reinicia o Jenkins para aplicar as permissões anteriormente adicionadas

# Rodar na instância app-server

docker rm -f $(docker ps -qa) # Finaliza todos os containers em execução
docker run -itd -p 80:3000 REPOSITORY_NAME/dcw-app:TAG # Inicializa o novo container com a versão especificada

# Instala pré-requisitos para serem usados posteriormente

sudo apt install ruby-full -y # Instala o Ruby
wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install # Faz o download do agente do codedeploy
chmod +x ./install # Da permissão de execução para o arquivo
sudo ./install auto > /tmp/logfile # executa a instalação do agente
sudo service codedeploy-agent status # Verifica se o agente foi iniciado corretamente
sudo reboot # Reinicia a maquina para garantir que o agente entre em execução
